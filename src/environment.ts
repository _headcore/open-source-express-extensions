import { config } from 'dotenv';
import * as fs from 'fs';
import { logger } from "./logger";

export function environment() {

    const envPath =  process.env.envPath;
    if (!envPath) return;

    const currentEnv = `${envPath}`;
    let foundEnv = fs.existsSync(currentEnv);
    
    if(!foundEnv) {
        const message = `Env ${process.env.NODE_ENV} not found! If a .env exists, it will be loaded!`;
        return logger('environment', message, 'warn');
    }
    
    config({ path: envPath });

    logger('environment', `Current: ${process.env.NODE_ENV}`);
}