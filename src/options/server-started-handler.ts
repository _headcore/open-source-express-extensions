export interface ServerStartedHandler {
    (port: string | number) : void;
}