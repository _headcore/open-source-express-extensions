"use strict";

export enum HttpStatus {

    /**
     * The server has received the request headers and the client should proceed to send the request body
     * (in the case of a request for which a body needs to be sent; for example, a post request).
     * Sending a large request body to a server after a request has been rejected for inappropriate headers would be inefficient.
     * To have a server check the request's headers, a client must send expect: 100-continue as a header in its initial request
     * and receive a 100 continue status code in response before sending the body. the response 417 expectation failed indicates the request should not be continued.
     */
    continue = 100,

    /**
     * The requester has asked the server to switch protocols and the server has agreed to do so.
     */
    switchingProtocols = 101,

    /**
     * A webdav request may contain many sub-requests involving file operations, requiring a long time to complete the request.
     * This code indicates that the server has received and is processing the request, but no response is available yet.
     * This prevents the client from timing out and assuming the request was lost.
     */
    processing = 102,

    /**
     * Standard response for successful http requests.
     * The actual response will depend on the request method used.
     * In a get request, the response will contain an entity corresponding to the requested resource.
     * In a post request, the response will contain an entity describing or containing the result of the action.
     */
    ok = 200,

    /**
     * The request has been fulfilled, resulting in the creation of a new resource.
     */
    created = 201,

    /**
     * The request has been accepted for processing, but the processing has not been completed.
     * The request might or might not be eventually acted upon, and may be disallowed when processing occurs.
     */
    accepted = 202,

    /**
     * Since http/1.1
     * The server is a transforming proxy that received a 200 ok from its origin,
     * but is returning a modified version of the origin's response.
     */
    nonAuthoritativeInformation = 203,

    /**
     * The server successfully processed the request and is not returning any content.
     */
    noContent = 204,

    /**
     * The server successfully processed the request, but is not returning any content.
     * Unlike a 204 response, this response requires that the requester reset the document view.
     */
    resetContent = 205,

    /**
     * The server is delivering only part of the resource (byte serving) due to a range header sent by the client.
     * The range header is used by http clients to enable resuming of interrupted downloads,
     * or split a download into multiple simultaneous streams.
     */
    partialContent = 206,

    /**
     * The message body that follows is an xml message and can contain a number of separate response codes,
     * depending on how many sub-requests were made.
     */
    multiStatus = 207,

    /**
     * The members of a dav binding have already been enumerated in a preceding part of the (multistatus) response,
     * and are not being included again.
     */
    alreadyReported = 208,

    /**
     * The server has fulfilled a request for the resource,
     * and the response is a representation of the result of one or more instance-manipulations applied to the current instance.
     */
    imUsed = 226,

    /**
     * Indicates multiple options for the resource from which the client may choose (via agent-driven content negotiation).
     * For example, this code could be used to present multiple video format options,
     * to list files with different filename extensions, or to suggest word-sense disambiguation.
     */
    multipleChoices = 300,

    /**
     * This and all future requests should be directed to the given uri.
     */
    movedPermanently = 301,

    /**
     * This is an example of industry practice contradicting the standard.
     * The http/1.0 specification (rfc 1945) required the client to perform a temporary redirect
     * (the original describing phrase was "moved temporarily"), but popular browsers implemented 302
     * with the functionality of a 303 see other. therefore, http/1.1 added status codes 303 and 307
     * to distinguish between the two behaviours. however, some web applications and frameworks
     * use the 302 status code as if it were the 303.
     */
    found = 302,

    /**
     * Since http/1.1
     * The response to the request can be found under another uri using a get method.
     * When received in response to a post (or put/delete), the client should presume that
     * the server has received the data and should issue a redirect with a separate get message.
     */
    seeOther = 303,

    /**
     * Indicates that the resource has not been modified since the version specified by the request headers if-modified-since or if-none-match.
     * In such case, there is no need to retransmit the resource since the client still has a previously-downloaded copy.
     */
    notModified = 304,

    /**
     * Since http/1.1
     * The requested resource is available only through a proxy, the address for which is provided in the response.
     * Many http clients (such as mozilla and internet explorer) do not correctly handle responses with this status code, primarily for security reasons.
     */
    useProxy = 305,

    /**
     * No longer used. Originally meant "subsequent requests should use the specified proxy."
     */
    switchProxy = 306,

    /**
     * Since http/1.1
     * In this case, the request should be repeated with another uri; however, future requests should still use the original uri.
     * In contrast to how 302 was historically implemented, the request method is not allowed to be changed when reissuing the original request.
     * For example, a post request should be repeated using another post request.
     */
    temporaryRedirect = 307,

    /**
     * The request and all future requests should be repeated using another uri.
     * 307 and 308 parallel the behaviors of 302 and 301, but do not allow the http method to change.
     * So, for example, submitting a form to a permanently redirected resource may continue smoothly.
     */
    permanentRedirect = 308,

    /**
     * The server cannot or will not process the request due to an apparent client error
     * (e.g., malformed request syntax, too large size, invalid request message framing, or deceptive request routing).
     */
    badRequest = 400,

    /**
     * Similar to 403 forbidden, but specifically for use when authentication is required and has failed or has not yet
     * been provided. The response must include a www-authenticate header field containing a challenge applicable to the
     * requested resource. See basic access authentication and digest access authentication. 401 semantically means
     * "unauthenticated",i.e. The user does not have the necessary credentials.
     */
    unauthorized = 401,

    /**
     * Reserved for future use. The original intention was that this code might be used as part of some form of digital
     * cash or micro payment scheme, but that has not happened, and this code is not usually used.
     * Google developers api uses this status if a particular developer has exceeded the daily limit on requests.
     */
    paymentRequired = 402,

    /**
     * The request was valid, but the server is refusing action.
     * The user might not have the necessary permissions for a resource.
     */
    forbidden = 403,

    /**
     * The requested resource could not be found but may be available in the future.
     * Subsequent requests by the client are permissible.
     */
    notFound = 404,

    /**
     * A request method is not supported for the requested resource;
     * For example, a get request on a form that requires data to be presented via post, or a put request on a read-only resource.
     */
    methodNotAllowed = 405,

    /**
     * The requested resource is capable of generating only content not acceptable according to the accept headers sent in the request.
     */
    notAcceptable = 406,

    /**
     * The client must first authenticate itself with the proxy.
     */
    proxyAuthenticationRequired = 407,

    /**
     * The server timed out waiting for the request.
     * According to http specifications:
     * "The client did not produce a request within the time that the server was prepared to wait. the client may repeat the request without modifications at any later time."
     */
    requestTimeout = 408,

    /**
     * Indicates that the request could not be processed because of conflict in the request,
     * such as an edit conflict between multiple simultaneous updates.
     */
    conflict = 409,

    /**
     * I that the resource requested is no longer available and will not be available again.
     * This should be used when a resource has been intentionally removed and the resource should be purged.
     * Upon receiving a 410 status code, the client should not request the resource in the future.
     * Clients such as search engines should remove the resource from their indices.
     * Most use cases do not require clients and search engines to purge the resource, and a "404 not found" may be used instead.
     */
    gone = 410,

    /**
     * the request did not specify the length of its content, which is required by the requested resource.
     */
    lengthRequired = 411,

    /**
     * The server does not meet one of the preconditions that the requester put on the request.
     */
    preconditionFailed = 412,

    /**
     * The request is larger than the server is willing or able to process. previously called "request entity too large".
     */
    payloadTooLarge = 413,

    /**
     * The uri provided was too long for the server to process. often the result of too much data being encoded as a query-string of a get request,
     * in which case it should be converted to a post request.
     * Called "request-uri too long" previously.
     */
    uriTooLong = 414,

    /**
     * The request entity has a media type which the server or resource does not support.
     * For example, the client uploads an image as image/svg+xml, but the server requires that images use a different format.
     */
    unsupportedMediaType = 415,

    /**
     * The client has asked for a portion of the file (byte serving), but the server cannot supply that portion.
     * For example, if the client asked for a part of the file that lies beyond the end of the file.
     * Called "requested range not satisfiable" previously.
     */
    rangeNotSatisfiable = 416,

    /**
     * The server cannot meet the requirements of the expect request-header field.
     */
    expectationFailed = 417,

    /**
     * This code was defined in 1998 as one of the traditional ietf april fools' jokes, in rfc 2324, hyper text coffee pot control protocol,
     * and is not expected to be implemented by actual http servers. The rfc specifies this code should be returned by
     * teapots requested to brew coffee. This http status is used as an easter egg in some websites, including google.com.
     */
    iAmATeapot = 418,

    /**
     * The request was directed at a server that is not able to produce a response (for example because a connection reuse).
     */
    misdirectedRequest = 421,

    /**
     * The request was well-formed but was unable to be followed due to semantic errors.
     */
    unprocessableEntity = 422,

    /**
     * The resource that is being accessed is locked.
     */
    locked = 423,

    /**
     * The request failed due to failure of a previous request (e.g., a proppatch).
     */
    failedDependency = 424,

    /**
     * The client should switch to a different protocol such as tls/1.0, given in the upgrade header field.
     */
    upgradeRequired = 426,

    /**
     * The origin server requires the request to be conditional.
     * Intended to prevent "the 'lost update' problem, where a client
     * gets a resource's state, modifies it, and puts it back to the server,
     * when meanwhile a third party has modified the state on the server, leading to a conflict."
     */
    preconditionRequired = 428,

    /**
     * The user has sent too many requests in a given amount of time. intended for use with rate-limiting schemes.
     */
    tooManyRequests = 429,

    /**
     * The server is unwilling to process the request because either an individual header field,
     * or all the header fields collectively, are too large.
     */
    requestHeaderFieldsTooLarge = 431,

    /**
     * A server operator has received a legal demand to deny access to a resource or to a set of resources
     * That includes the requested resource. the code 451 was chosen as a reference to the novel fahrenheit 451.
     */
    unavailableForLegalReasons = 451,

    /**
     * A generic error message, given when an unexpected condition was encountered and no more specific message is suitable.
     */
    internalServerError = 500,

    /**
     * The server either does not recognize the request method, or it lacks the ability to fulfill the request.
     * Usually this implies future availability (e.g., a new feature of a web-service api).
     */
    notImplemented = 501,

    /**
     * The server was acting as a gateway or proxy and received an invalid response from the upstream server.
     */
    badGateway = 502,

    /**
     * The server is currently unavailable (because it is overloaded or down for maintenance).
     * Generally, this is a temporary state.
     */
    serviceUnavailable = 503,

    /**
     * The server was acting as a gateway or proxy and did not receive a timely response from the upstream server.
     */
    gatewayTimeout = 504,

    /**
     * The server does not support the http protocol version used in the request
     */
    httpVersionNotSupported = 505,

    /**
     * Transparent content negotiation for the request results in a circular reference.
     */
    variantAlsoNegotiates = 506,

    /**
     * The server is unable to store the representation needed to complete the request.
     */
    insufficientStorage = 507,

    /**
     * The server detected an infinite loop while processing the request.
     */
    loopDetected = 508,

    /**
     * Further extensions to the request are required for the server to fulfill it.
     */
    notExtended = 510,

    /**
     * The client needs to authenticate to gain network access.
     * Intended for use by intercepting proxies used to control access to the network (e.g., "captive portals" used
     * To require agreement to terms of service before granting full internet access via a wi-fi hotspot).
     */
    networkAuthenticationRequired = 511
}