import cors = require("cors");

export interface Cors {
    enabled: boolean;
    options?: cors.CorsOptions | cors.CorsOptionsDelegate;
}   
