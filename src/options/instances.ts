export interface Instances {
    [controllerKey: string]: { [controllerVersion: string]: any } 
}