import { ControllersPath } from "./controllers-path";

export interface ControllersOptions {
    paths: ControllersPath[];
    verbose?: boolean;
}
