export interface VersionDirectoryHandler {
    (versionDirectory: string, versionName: string): void;
}