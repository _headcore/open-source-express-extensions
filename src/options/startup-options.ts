import { Cors } from './cors';
import { BodyParser } from './body-parser';
import { Consign } from './consign';
import { Request, Response } from 'express';
import { ServerStartedHandler } from '.';
import { EnvOptions } from './env-options';
import { IHelmetConfiguration } from 'helmet';
import { Express } from 'express';
import { ApiError } from '../errors';

export interface StartupOptions {
    cwd: string;
    env: EnvOptions;
    helmet?: IHelmetConfiguration;
    parsers?: BodyParser;
    cors?: Cors;
    autoLoad?: Consign;
    errorHandler?: (request: Request, response: Response, error: ApiError<any>) => void;
    startupHandler?: (app: Express) => Promise<void> | void;
    startedHandler: ServerStartedHandler;
    port?: number;
}