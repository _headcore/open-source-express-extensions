import { ConsignOptions } from "./consign-options";

export interface Consign {
    includes: string[];
    excludes?: string[];
    options?: ConsignOptions;
}