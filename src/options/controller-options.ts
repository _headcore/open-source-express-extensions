export interface ControllerOptions {
    prefix: string;
    version: number;
    middlewares?: any[];
    replaceOnRoute?: {
        [key: string]: string
    }
}