export interface ConsignOptions {
    cwd: string,
    locale: string,
    logger: any,
    verbose: boolean,
    extensions: string[],
    loggingType: string
}