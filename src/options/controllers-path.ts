export interface ControllersPath {
    cwd: string;
    controllers?: {
        [versionKey: string]: string[]
    }
}