import bodyParser = require("body-parser");

export interface BodyParser {
    raw?: { enabled: boolean, options?: bodyParser.Options }
    text?: { enabled: boolean, options?: bodyParser.OptionsText }
    json?: { enabled: boolean, options?: bodyParser.OptionsJson }
    urlEncoded?: { enabled: boolean, options?: bodyParser.OptionsUrlencoded }
}
