import { Response, Request } from 'express';

export interface ExpressOptions {
    tryCatchMethods: string[];
    errorHandler: EndPointErrorHandler;
    port?: number;
}

export interface EndPointErrorHandler {
    (request: Request, response: Response, error: Error) : void;
}