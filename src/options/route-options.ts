import { HttpMethod } from "./http-method";

export interface RouteOptions {
    method: HttpMethod;
    path?: string;
    customPrefix?: string;
    disabled?: boolean;
}