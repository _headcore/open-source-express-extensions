
export interface ControllerFile {
    version: string;
    name: string;
    path: string;
}