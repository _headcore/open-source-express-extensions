export interface RoutePath {
    controller: string;
    version: string;
    method: string;
}