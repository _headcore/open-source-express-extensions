export interface ControllerFileHandler {
    (versionName: string, controllerName: string, controllerFilePath: string): void;
}
