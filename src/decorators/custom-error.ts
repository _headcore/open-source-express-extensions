export function CustomError(message: string) {

    return function (target: any, propertyKey: string, descriptor: PropertyDescriptor) {
        
        // Original method
        if(descriptor === undefined) {
            descriptor = Object.getOwnPropertyDescriptor(target, propertyKey);
        }

        const originalMethod = descriptor.value;

        // Try method
        descriptor.value = function(...args: any[]) {

            try {
                return originalMethod.apply(this, args);
            } catch (error) {
                const customError = new Error(message) as any;
                customError.method = propertyKey;
                customError.inner = error;
                customError.args = args;
                throw customError;
            }
        }
    }
}