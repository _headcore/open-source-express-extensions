import 'reflect-metadata';
import { ControllerOptions } from "..";

export function Controller(value: ControllerOptions) {
    return function (target: Function) {
        Reflect.defineMetadata("custom:anotations:controller", value, target);
    }
}