import 'reflect-metadata';
import { RouteOptions } from "..";

export function Route(options: RouteOptions) {
    return function(target: Object, propertyKey: string | symbol) {
        Reflect.defineMetadata('custom:anotations:route', options, target, propertyKey);
    }
}