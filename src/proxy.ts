import { Express, Request, Response, NextFunction } from 'express';
import { InternalServerError } from './errors';
import { httpMethods } from './options';

export interface ErrorHandler<T> {
    (request: Request, response: Response, error: T): void | Promise<void>;
}
export function proxy<T = InternalServerError>(express: Express, errorHandler: ErrorHandler<T>) {
    const customHandler = function (handler: any) {
        return async function (request: Request, response: Response, next: NextFunction) {
            try {
                await handler.apply(this, [request, response, next]);
            } catch (error) {
                if (!error?.status) {
                    const apiError = new InternalServerError();
                    apiError.internalMessage = error.message;
                    error = apiError;
                }
                await errorHandler(request, response, error);
            }
        }
    }
    const getParam = function(args: any) {
        if(Array.isArray(args)) {
            const customArgs: any[] = [];
            for (const arg of args) {
                if(typeof arg === 'function') {
                    customArgs.push(customHandler(arg));
                } else {
                    customArgs.push(arg);
                }
            }
            return customArgs;
        }
        if(typeof args === 'function') {
            return customHandler(args);
        }
        return args;
    }
    return new Proxy(express, {
        get: (target, property, receiver) => {
            const originalHandler = Reflect.get(target, property, receiver);
            const isIntercepted = httpMethods.find((httpMethod) => httpMethod === property);
            if(isIntercepted) {
                return function(...params: any[]) {
                    const customParams: any[] = [];
                    for (const param of params) {
                        customParams.push(getParam(param));
                    }
                    return originalHandler.apply(this, customParams);
                }
            }
            return originalHandler;
        }
    });
}