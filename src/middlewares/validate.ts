import { ValidationChain, validationResult } from 'express-validator';
import { Request, Response, NextFunction } from 'express';
import { HttpStatus } from '..';

export const validate = (validations: ValidationChain[], status: HttpStatus = HttpStatus.badRequest) => {
    return [validations, (request: Request, response: Response, next: NextFunction) => {
        const result = validationResult(request);
        if(!result.isEmpty()) {
            response
            .status(status)
            .json({
                errors: result.array()
            });
        } else {
            next();
        }
    }];
}