import { ValidationChain } from "express-validator";

export function composeValidators(...validations: ValidationChain[][]) {
    return validations.reduce((p, c, i) => p.concat(c));
}