export const controllerTemplate = (name: string, version: string, prefix: string) => `import { Request, Response } from 'express';
import { Route, Controller, HttpStatus } from '@headcore/express-extensions';

@Controller({
    prefix: '${prefix}',
    version: ${version}
})
export class ${name}Controller {

    @Route({ method: 'post' })
    create = [
        async (request: Request, response: Response) => {
            response.status(HttpStatus.created).json({ Method: 'POST' });
        } 
    ];

    @Route({ method: 'get', path: '/:id' })
    getById = [
        async (request: Request, response: Response) => {
            response.status(HttpStatus.ok).json({ Method: 'GET' });
        } 
    ];

    @Route({ method: 'get' })
    getAll = [
        async (request: Request, response: Response) => {
            response.status(HttpStatus.ok).json({ Method: 'GET' });
        } 
    ];

    @Route({ method: 'put', path: '/:id' })
    edit = [
        async (request: Request, response: Response) => {
            response.status(HttpStatus.ok).json({ Method: 'PUT' });
        } 
    ];

    @Route({ method: 'delete', path: '/:id' })
    delete = [
        async (request: Request, response: Response) => {
            response.status(HttpStatus.ok).json({ Method: 'DELETE' });
        } 
    ];
}`