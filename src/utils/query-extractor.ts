import { Request } from 'express';

export function queryExtractor<T>(request: Request, toNumberProperties?: string[]) {
    const query = request.query as any; 

    for (const property of toNumberProperties) {
        query[property] = Number.parseInt(query[property]);
    }

    return query as T;
}