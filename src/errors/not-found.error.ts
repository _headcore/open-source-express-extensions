import { HttpStatus } from "../options";
import { ApiError } from "./api.error";

export class NotFoundError<T = any> extends ApiError<T> {
    status = HttpStatus.notFound;
}