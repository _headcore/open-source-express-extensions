import { HttpStatus } from "../options";
import { ApiError } from "./api.error";

export class InternalServerError<T = any> extends ApiError<T> {
    status = HttpStatus.internalServerError;
}