import { HttpStatus } from "../options";
import { ApiError } from "./api.error";

export class ForbiddenError<T = any> extends ApiError<T> {
    status = HttpStatus.forbidden;
}