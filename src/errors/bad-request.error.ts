import { HttpStatus } from "../options";
import { ApiError } from "./api.error";

export type BadRequestErrorType = {
    location: string;
    param: string;
    msg: string;
};
export class BadRequestError extends ApiError<BadRequestErrorType> {
    status = HttpStatus.badRequest;
}