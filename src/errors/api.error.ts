import { HttpStatus } from "../options";

export type ApiErrorOptions<T> = {
    internalMessage?: string; 
    errors?: T[]; 
    message?: string;
    status?: HttpStatus;
}
export class ApiError<T> extends Error {

    status: number;
    internalMessage?: string;
    errors?: T[];

    constructor(options?: ApiErrorOptions<T>) {
        super()
        Object.assign(this, options);
    }

    getPublicData() {
        const error = {} as any;
        if (this.message) {
            error.message = this.message;
        }
        if (this.errors) {
            error.errors = this.errors;
        }
        return error;
    }
}