import { HttpStatus } from "../options";
import { ApiError } from "./api.error";

export class UnauthorizedError<T = any> extends ApiError<T> {
    status = HttpStatus.unauthorized;
}