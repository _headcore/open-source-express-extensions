import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as cors from 'cors';
import { controllers } from './controllers';
import { StartupOptions } from '.';
import { environment } from './environment';
import * as helmet from 'helmet';
import { logger } from './logger';
import { proxy } from './proxy';
import { HttpStatus } from './options';

export async function startup(options: StartupOptions): Promise<express.Express> {

    // Load env
    environment();

    // invoke express
    const server = _getExpress(options);
    
    // Security settings
    if(process.env.NODE_ENV === options.env.prodEnv) {
        server.use(helmet(options.helmet));
        server.set('trust proxy', 1);
    }

    // configure unions options
    if(options) {
        _configureOptions(server, options);
    } 

    // port definition
    const serverPort = options && options.port ? options.port : undefined;
    let port = Number.parseInt(process.env.PORT) || serverPort || 3000;

    // start Server

    // Load hander
    if(options.startupHandler) {
        await options.startupHandler(server);
    }

    // Load controllers
    controllers(server, options.cwd);

    // Listen
    _listen(server, port, options.startedHandler);

    return server;
}

function _listen(server: express.Express, port: number, handler: any): any {
    return server
        .listen(port, () => handler(port))
        .on('error', (error) => {
            const portInUse = error.message.startsWith('listen EADDRINUSE');
            if(portInUse) {
                port++;
                return _listen(server, port, handler)
            }
        })
}

function _getExpress(options: StartupOptions) : express.Express {

    return proxy(
        express(), 
        (request, response, error) => {
            if (options.errorHandler) {
                options.errorHandler(request, response, error);
            } else {
                response.sendStatus(HttpStatus.internalServerError);
            }
        }, 
    );
}

function _configureOptions(express: express.Express, options?: StartupOptions) {

    if(options.parsers) {
        if(options.parsers.raw && options.parsers.raw.enabled) {
            express.use(bodyParser.raw(options.parsers.raw.options));
            logger('startup', 'Parser: raw enabled!');
        }
        if(options.parsers.text && options.parsers.text.enabled) {
            express.use(bodyParser.text(options.parsers.text.options));
            logger('startup', 'Parser: text enabled!');

        }
        if(options.parsers.json && options.parsers.json.enabled) {
            express.use(bodyParser.json(options.parsers.json.options));
            logger('startup', 'Parser: json enabled!');
        }
        if(options.parsers.urlEncoded && options.parsers.urlEncoded.enabled) {
            express.use(bodyParser.urlencoded(options.parsers.urlEncoded.options));
            logger('startup', 'Parser: urlEncoded enabled!');
        }
    }

    if(options.cors && options.cors.enabled) {
        express.use(cors(options.cors.options));
        logger('startup', 'Cors: enabled!');
    } 

    if(options.autoLoad) {
        const consign = require('consign')(options.autoLoad.options);
        options.autoLoad.includes.forEach(include => consign.include(include));
        if(options.autoLoad.excludes) {
            options.autoLoad.excludes.forEach(exclude => consign.exclude(exclude));
        }
        consign.into(express);
    }
}