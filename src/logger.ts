import moment = require("moment");

export function logger(method: string, message: string, type: 'warn' | 'log' = 'log') { 
    if(process.env.VERBOSE?.toLocaleLowerCase() === "true") {
        const log = console[type];
        log(`[${moment().format('hh:mm:ss')}][${method}] ${message}`);
    }
}