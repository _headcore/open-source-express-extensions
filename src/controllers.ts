import * as fs from 'fs';
import { RouteOptions, ControllerOptions, ControllersPath, ControllerFile } from ".";
import { Express } from 'express';
import { logger } from './logger';
 
export function controllers(router: Express, cwd: string) {

    const configs = require(`${cwd}/app.config.json`);
    const controllersDir = `${cwd}/dist/${configs.controllersDirectory.replace('src', '')}`;

    // Paths
    if(fs.existsSync(controllersDir)) {
        
        // foreach version directory
        for (const controllerFile of _getControllersFiles({ cwd: controllersDir })) {

            const exists = fs.existsSync(`${controllerFile.path}.js`);
            if(!exists) {
                console.error(`Controller file not found: ${controllerFile.version}/${controllerFile.name}`);
                continue;
            }

            // node require
            const nodeRequire = require(controllerFile.path);
            if(!nodeRequire) {
                console.error(`Controller not found: ${controllerFile.name}`);
                return;
            };
            
            // object and instance
            const properties = Object.getOwnPropertyNames(nodeRequire);
            const controllerNameReplaced = controllerFile.name.replace(/[-.]/g, '').toUpperCase();
            const controllerName = properties.find((p) => p.toUpperCase() === controllerNameReplaced);
            const object = nodeRequire[controllerName];

            if(object) {
                const instance = new object();
                _loadController(router, instance);
            }
        }
    }
}

function _loadController(router: Express, instance: any) {

    const methods = Object.getOwnPropertyNames(instance);
    const controllerOptions: ControllerOptions = _getControllerOptions(instance);

    if(!controllerOptions) {
        return;
    }

    for (const method of methods) {
                
        const propertyAnotations = _getPropertyAnotations(instance, method);

        // Get is route
        if(propertyAnotations && 
            propertyAnotations.length > 0) {
            
            // Options
            const routeOptions: RouteOptions = propertyAnotations[0];
            if (routeOptions.disabled) continue;    

            // Define route
            const route =  _defineRoute(routeOptions, controllerOptions);

            // Controller Middlewares
            if(controllerOptions.middlewares)  { 
                instance[method] = instance[method].concat(controllerOptions.middlewares);
                instance[method] = [].concat(controllerOptions.middlewares).concat(instance[method]);
            }

            router[routeOptions.method](route, instance[method]);
            logger('controllers', `Route loaded: ${routeOptions.method} ${route}`);
        }
    }
}

function _defineRoute(routeOptions: RouteOptions, controllerOptions: ControllerOptions) {

    const path = routeOptions.path ? routeOptions.path : "";
    const prefix = routeOptions.customPrefix 
        ? routeOptions.customPrefix 
        : controllerOptions.prefix;
     
    let route = `/api/v${controllerOptions.version}/${prefix}${path}`;
    
    // Replaces
    if (controllerOptions.replaceOnRoute) {
        const replaces = Object.getOwnPropertyNames(controllerOptions.replaceOnRoute);
        for (const replace of replaces) {
            route = route.replace(new RegExp(replace), controllerOptions.replaceOnRoute[replace]);
        }
    }

    return route;
}

function _getControllersFiles(path: ControllersPath) {

    const controllersFiles: ControllerFile[] = [];
    const versionNames = path.controllers 
        ? Object.getOwnPropertyNames(path.controllers)
        : fs.readdirSync(path.cwd).filter(fileOrDir => fs.statSync(`${path.cwd}/${fileOrDir}`).isDirectory());

    for (const versionName of versionNames) {

        const versionDirectory = `${path.cwd}/${versionName}`;
        
        let controllerNames = path.controllers 
            ? path.controllers[versionName]
            : fs.readdirSync(versionDirectory).filter(fileOrDir => fs.statSync(`${versionDirectory}/${fileOrDir}`).isFile());

        controllerNames = !path.controllers ? controllerNames.filter(cn => cn.endsWith('.js')) : controllerNames;
        controllerNames = !path.controllers ? controllerNames.map(cn => cn.replace('.js', '')) : controllerNames;

        for (const controllerName of controllerNames) {
            const controllerFilePath = `${versionDirectory}/${controllerName}`;
            controllersFiles.push({
                name: controllerName,
                version: versionName,
                path: controllerFilePath
            });
        }
    }


    return controllersFiles;
}

function _getControllerOptions(target: object) {
    return Reflect.getMetadata("custom:anotations:controller", target.constructor);
}

function _getPropertyAnotations(target: any, propertyKey: string | symbol) {

     // get info about keys that used in current property
     const keys: any[] = Reflect.getMetadataKeys(target, propertyKey);
     const decorators = keys
       // filter your custom decorators
       .filter(key => key.toString().startsWith("custom:anotations"))
       .reduce((values, key) => {
         // get metadata value.
         const currValues = Reflect.getMetadata(key, target, propertyKey);
         return values.concat(currValues);
       }, []);
   
     return decorators;
}